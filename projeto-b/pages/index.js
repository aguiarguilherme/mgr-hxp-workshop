import Head from 'next/head';
import { Button } from 'workshop-modalgr-ui-components';

export default function Home() {
  return (
    <div className="min-h-screen">
      <Head>
        <title>Projeto B</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="min-h-screen flex items-center justify-center gap-2">
        <Button>Sou um botão no projeto B</Button>
      </div>
    </div>
  );
}
