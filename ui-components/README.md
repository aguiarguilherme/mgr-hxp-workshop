
# ui-components

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

Este repositório é responsável por gerenciar os componentes a usar no `client`. Com isso, podemos usar de forma padronizada toda a parte de UI entre projetos.

## Especificações

[WIP]

## Comandos essenciais

> **Atenção**: Os comandos disponíveis estão assumindo que o `yarn` está sendo usado. Para usar com npm basta substituir por `npm run`.

### yarn build
Transpila a biblioteca pra cjs e esm, permitindo ser usado em praticamente todo tipo de projeto-client

### yarn storybook
Analisa o projeto e gera um storybook com todos os stories de componentes, disponivel em `http://localhost:6006`.

### storybook:export
Compila o projeto pra arquivos estáticos de HTML, permitindo visualizar sem necessidade de um servidor rodando constantemente (perde o auto-refresh, no entanto)


## Contribuindo

[WIP]
