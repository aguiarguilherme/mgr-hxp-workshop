/* eslint-disable import/prefer-default-export */
import fileDownload from 'js-file-download';

export function jsonToCsv(items: any) {
  const replacer = (key: any, value: any) => (value === null ? '' : value);
  const header = Object.keys(items[0]);
  const csv = [
    header.join(','),
    ...items.map((row: any) =>
      header
        .map((fieldName) => JSON.stringify(row[fieldName], replacer))
        .join(',')
    ),
  ].join('\r\n');
  return csv;
}

export function downloadCsv(data: any, filename: string) {
  fileDownload(jsonToCsv(data), filename);
}
