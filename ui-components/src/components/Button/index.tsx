import React, { ButtonHTMLAttributes } from 'react';

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {}

function Button({ children, onClick, ...props }: ButtonProps) {
  return (
    <button
      style={{ backgroundColor: 'red', padding: '4px 8px' }}
      onClick={(event) => {
        console.group('Ação de clique no botão foi executada:');
        console.log(
          'Oi, estou sendo enviado pelo componente na biblioteca! Timestamp:',
          Date.now()
        );
        onClick(event);
        console.groupEnd();
      }}
      {...props}
    >
      {children}
    </button>
  );
}

export default Button;
