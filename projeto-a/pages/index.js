import Head from 'next/head';
import { Button } from 'workshop-modalgr-ui-components';

export default function Home() {
  return (
    <div className="min-h-screen">
      <Head>
        <title>Projeto A</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="min-h-screen flex items-center justify-center gap-2">
        <button
          onClick={() => {
            console.log('Oi, sou um botão simples!');
          }}
        >
          Oi, sou um botão comum!
        </button>
        <Button
          onClick={() => {
            console.log('Oi, sou um botão turbinado!');
          }}
        >
          Oi, sou um botão turbinado!
        </Button>
      </div>
    </div>
  );
}
